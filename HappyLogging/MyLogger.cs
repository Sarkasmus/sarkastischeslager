﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HappyLogging
{
    class MyLogger : ILogger
    {
        private Program.LogLevel plevel;
        private string speicherort;

        public MyLogger(Program.LogLevel iNFO, string m)
        {
            plevel = iNFO;
            speicherort = m;
        }

        public void WriteMessage(string category, Program.LogLevel level, string message)
        {
            if(/*plevel >= Convert.ToInt32(level)*/ plevel >= level)
            {

            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(speicherort, true))
            {
                file.WriteLine(category + " " + message);
            }

            }

        }
    }
}
