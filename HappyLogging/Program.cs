﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HappyLogging
{
    class Program
    {
        public enum LogLevel
        {
            FATAL = 0,
            ERROR = 1,
            WARN = 2,
            INFO = 3,
            VERBOSE = 4
        }

        static void Main(string[] args)
        {
            ILogger logger = new MyLogger(LogLevel.INFO, @"C:\Users\2chit\Desktop\mylog.txt");

            Console.WriteLine("***Mein cooles Programm2***");
            try
            {
                int meineeingabe = 0;
                int ergebnis = 5 / meineeingabe;
            }
            catch (DivideByZeroException e)
            {
                logger.WriteMessage("EINGABEFEHLER:", LogLevel.ERROR, "Division durch 0");
            }

            double berechung = Math.PI / 3.0;
            logger.WriteMessage("BERECHNUNG:", LogLevel.VERBOSE, "Math.PI/3.0 =" + " " + berechung);
        }
    }
}
